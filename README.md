# ModelBoxer
A small OpenSCAD script to create brackets for miniature models with round bases, in order to store them in boxes.
You can add pylons to stack the brackets. An in between layer will be needed since the generated model is has holes.

Just change the parameters of the main file to suit your needs. The current parameters are an example, for storing WH40K Canoptek Scarab Swarms (small model with 40mm base) in a Prusament box (210mm long).

The layer between brackets could be magnetic, which can help with transporting miniatures.
