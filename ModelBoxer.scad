/// PARAMETERS
l = 210; // Length of the box
diam = 40; // Diameter of the base
min_border = 2; // Minimum border 
h = 15; // Max height of the models
nr_pylons = 3; // Number of pylons

/// PROGRAM, DO NOT CHANGE
w = diam + 2 * min_border;
nr_holes = floor((l - 2 * min_border)/diam);
p = nr_pylons-1;
hs = nr_holes-1;
between = (l - (nr_holes * diam) - (2 * min_border))/hs;
difference() {
    intersection() {
        union() {
            //Base
            cube([l,w, 1.5]);
            //Pylons
            for (i = [0 : 1 : p]) {
                translate([l*i/p, 0, 0]) cylinder(h, 8, 4);
                translate([l*i/p, w, 0]) cylinder(h, 8, 4);
            }
        }
        //Outline
        cube([l,w, h]);
    }
    //Holes
    for (i = [0 : 1 : hs])
        translate([i * (diam + between) + min_border + diam/2, w/2, -1])
            cylinder(2*h,diam/2,diam/2);
}
